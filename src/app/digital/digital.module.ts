import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DigitalRoutingModule } from './digital-routing.module';
import { DigitalComponent } from './digital/digital.component';


@NgModule({
  declarations: [
    DigitalComponent
  ],
  imports: [
    CommonModule,
    DigitalRoutingModule
  ]
})
export class DigitalModule { }
