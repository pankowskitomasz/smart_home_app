import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DigitalComponent } from './digital/digital.component';

const routes: Routes = [
  {
    path: "digital",
    component: DigitalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DigitalRoutingModule { }
