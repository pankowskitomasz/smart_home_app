import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccesscontrolComponent } from './accesscontrol/accesscontrol.component';

const routes: Routes = [
  {
    path: "access-control",
    component: AccesscontrolComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccesscontrolRoutingModule { }
