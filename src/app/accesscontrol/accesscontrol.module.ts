import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccesscontrolRoutingModule } from './accesscontrol-routing.module';
import { AccesscontrolComponent } from './accesscontrol/accesscontrol.component';


@NgModule({
  declarations: [
    AccesscontrolComponent
  ],
  imports: [
    CommonModule,
    AccesscontrolRoutingModule
  ]
})
export class AccesscontrolModule { }
